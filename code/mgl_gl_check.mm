#include "mgl_gl_check.h"

/*
internal long
GetDictionaryLong(CFDictionaryRef RefDictionary, CFStringRef Key) {
  long DictionaryValue;

  CFNumberRef DictionaryValueRef =
    (CFNumberRef) CFDictionaryGetValue(RefDictionary, Key);
  if (!DictionaryValueRef) {
    return -1;
  } else if (!CFNumberGetValue(DictionaryValueRef,
              kCFNumberLongType, &DictionaryValue)) {
    return -1;
  }

  return DictionaryValue;
}

internal double
GetDictionaryDouble(CFDictionaryRef RefDictionary, CFStringRef Key) {
  double DictionaryValue;

  CFNumberRef DictionaryValueRef =
    (CFNumberRef) CFDictionaryGetValue(RefDictionary, Key);
  if (!DictionaryValueRef) {
    return -1;
  } else if (!CFNumberGetValue(DictionaryValueRef,
              kCFNumberDoubleType, &DictionaryValue)) {
    return -1;
  }

  return DictionaryValue;
}
*/

unsigned char
mgl_HaveOpenGLCapabilitiesChanged(mgl_gl_capabilities *DisplayCapabilities, CGDisplayCount DisplayCount) {
  CGDisplayCount MaxDisplays = MAX_DISPLAY_COUNT;
  CGDirectDisplayID ActiveDisplays[MAX_DISPLAY_COUNT];

  if (DisplayCapabilities == NULL) {
    return 1;
  }

  CGDisplayCount NewDisplayCount = 0;
  CGDisplayErr DisplayError =
    CGGetActiveDisplayList(MaxDisplays, ActiveDisplays, &NewDisplayCount);

  if (DisplayError) return 1;
  if (DisplayCount != NewDisplayCount) return 1;

  short DisplayIndex;
  for (DisplayIndex = 0;
       DisplayIndex < DisplayCount;
       DisplayIndex++) {
    CGDirectDisplayID CurrentActiveDisplay = ActiveDisplays[DisplayIndex];
    mgl_gl_capabilities CurrentDisplayCapabilities = DisplayCapabilities[DisplayIndex];

    if (CurrentDisplayCapabilities.CGDisplayID != CurrentActiveDisplay)
      return 1;
    /* if (CurrentDisplayCapabilities.CGLDisplayMask != CGDisplayIDToOpenGLDisplayMask(CurrentActiveDisplay)) */
    /*   return 1; */

    // it may be better to use AppKit for this information!
    // the following work for macOS 10.6+
    CGDisplayModeRef ActiveDisplayMode = CGDisplayCopyDisplayMode(CurrentActiveDisplay);
    CGOpenGLDisplayMask ActiveDisplayMask = CGDisplayIDToOpenGLDisplayMask(CurrentActiveDisplay);

    if (CurrentDisplayCapabilities.DeviceWidth != CGDisplayModeGetWidth(ActiveDisplayMode))
      return 1;
    if (CurrentDisplayCapabilities.DeviceHeight != CGDisplayModeGetHeight(ActiveDisplayMode))
      return 1;

    /*
     * the following is deprecated after 10.11
     * use NSScreen to get pixel depth and other information
     */
    // if (CurrentDisplayCapabilities.DevicePixelDepth != CGDisplayModeCopyPixelEncoding(CurrentActiveDisplay);

    // get renderer info based on gDevice
    CGLRendererInfoObj RendererInformation;
    long DeviceVRAMSize = 0; // in bytes
    int RendererNumber = 0;
    unsigned long RendererID; // renderer ID
    CGLError GLError;

    GLError = CGLQueryRendererInfo(ActiveDisplayMask, &RendererInformation, &RendererNumber);

    if (GLError == 0) {
      CGLDescribeRenderer(RendererInformation, 0, kCGLRPRendererCount, &RendererNumber);
      for (long RendererIndex = 0;
           RendererIndex < RendererNumber;
           RendererIndex++) {
        // find the accelerated renderer - hardware accelerated?
        long AcceleratedRenderer = 0;
        CGLDescribeRenderer(RendererInformation, RendererIndex, kCGLRPAccelerated, (int *) &AcceleratedRenderer);

        if (AcceleratedRenderer != 0) {
          CGLDescribeRenderer(RendererInformation, RendererIndex, kCGLRPRendererID, (int *) &RendererID);
          if (RendererID != DisplayCapabilities[DisplayIndex].RendererID) return 1;

          CGLDescribeRenderer(RendererInformation, RendererIndex, kCGLRPVideoMemoryMegabytes, (int *) &DeviceVRAMSize);
          if (DisplayCapabilities[DisplayIndex].DeviceVRAMSize != (DeviceVRAMSize * 1024 * 1024)) return 1;

          break;
        }
      }

      CGLDestroyRendererInfo(RendererInformation);
    }
  }

  /*
  NSScreen *MainScreen = [NSScreen mainScreen];

  if (MainScreen == nil) {
    return 0;
  }

  // TODO(antonio): why does NSBitsPerPixelFromDepth return 0
  NSWindowDepth MainScreenPixelDepthQuery = [MainScreen depth];
  short MainScreenPixelDepth = 0;

  if (MainScreenPixelDepthQuery == APPLE_24_BIT_DEPTH_RGB) {
    MainScreenPixelDepth = 24;
  } else if (MainScreenPixelDepthQuery == APPLE_64_BIT_DEPTH_RGB) {
    MainScreenPixelDepth = 64;
  } else if (MainScreenPixelDepthQuery == APPLE_128_BIT_DEPTH_RGB) {
    MainScreenPixelDepth = 128;
  } else {
    return 0;
  }

  NSLog(@"glGetString: %d\n", GL_EXTENSIONS);

  */
  return 1;
}

void
mgl_CheckOpenGLCapabilities(CGDisplayCount MaxDisplays,
                            mgl_gl_capabilities *DisplayCapabilities,
                            CGDisplayCount *DisplayCount) {
  CGDirectDisplayID DisplayIDs[MAX_DISPLAY_COUNT];
  CGDisplayErr DisplayError;

  *DisplayCount = 0;
  
  if (MaxDisplays == 0) {
    DisplayError = CGGetActiveDisplayList(MAX_DISPLAY_COUNT, DisplayIDs, DisplayCount);
    if (DisplayError) {
      *DisplayCount = 0;
    }
    memset(DisplayIDs, 0, sizeof(CGDirectDisplayID) * (*DisplayCount));
    return;
  }

  if (DisplayCapabilities == NULL) {
    return;
  }

  DisplayError = CGGetActiveDisplayList(MaxDisplays, DisplayIDs, DisplayCount);

  if (DisplayError) {
    return;
  }
  if (*DisplayCount == 0) {
    return;
  }

  memset(DisplayCapabilities, 0, sizeof(DisplayCapabilities) * (*DisplayCount));

  CGDisplayCount DisplayNumber = *DisplayCount;
  for (unsigned int DisplayIndex = 0;
       DisplayIndex < DisplayNumber;
       DisplayIndex++) {
    CGDirectDisplayID CurrentDisplayID = DisplayIDs[DisplayIndex];
    CGOpenGLDisplayMask CurrentDisplayMask = CGDisplayIDToOpenGLDisplayMask(DisplayIDs[DisplayIndex]);

    DisplayCapabilities[DisplayIndex].CGDisplayID = CurrentDisplayID; 

    CGDisplayModeRef CurrentDisplayMode = CGDisplayCopyDisplayMode(CurrentDisplayID);

    DisplayCapabilities[DisplayIndex].DeviceWidth = CGDisplayModeGetWidth(CurrentDisplayMode);
    DisplayCapabilities[DisplayIndex].DeviceHeight = CGDisplayModeGetHeight(CurrentDisplayMode);

    // TODO(antonio): Get pixel depth
    // DisplayCapabilities[DisplayIndex].DevicePixelDepth = CGDisplayModeGetHeight(CurrentDisplayID);
    DisplayCapabilities[DisplayIndex].DeviceRefreshRate = (short) CGDisplayModeGetRefreshRate(CurrentDisplayMode);

    CGLRendererInfoObj RendererInformation;
    int RendererNumber = 0;
    CGLError GLError = kCGLNoError;

    GLError = CGLQueryRendererInfo(CurrentDisplayMask, &RendererInformation, &RendererNumber);
    
    if (GLError == 0) {
      CGLDescribeRenderer(RendererInformation, 0, kCGLRPRendererCount, &RendererNumber);
      for (int RendererIndex = 0; RendererIndex < RendererNumber; RendererIndex++) {
        int AcceleratedRenderer = 0;
        CGLDescribeRenderer(RendererInformation, RendererIndex, kCGLRPAccelerated, &AcceleratedRenderer);

        // assume one accelerated renderer
        if (AcceleratedRenderer != 0) {
          CGLDescribeRenderer(
            RendererInformation,
            RendererIndex,
            kCGLRPRendererID,
            (int *) &DisplayCapabilities[DisplayIndex].RendererID);

          int FullScreenCapable;
          CGLDescribeRenderer(RendererInformation, RendererIndex, kCGLRPFullScreen, &FullScreenCapable);
          DisplayCapabilities[DisplayIndex].FullScreenCapable = (bool) FullScreenCapable;

          int VRAMSizeInMegabytes = 0;
          CGLDescribeRenderer(RendererInformation, RendererIndex, kCGLRPVideoMemoryMegabytes, &VRAMSizeInMegabytes);
          DisplayCapabilities[DisplayIndex].DeviceVRAMSize = VRAMSizeInMegabytes * 1024 * 1024;

          int TextureRAMSizeInMegabytes = 0;
          CGLDescribeRenderer(RendererInformation, RendererIndex, kCGLRPTextureMemoryMegabytes, &TextureRAMSizeInMegabytes);
          DisplayCapabilities[DisplayIndex].DeviceTextureRAMSize = TextureRAMSizeInMegabytes * 1024 * 1024;
          break;
        }
      }
    }

    CGLDestroyRendererInfo(RendererInformation);

    // build context information
    CGLPixelFormatAttribute PixelFormatAttributes[] = {
      kCGLPFADisplayMask,
      (CGLPixelFormatAttribute) CurrentDisplayMask, // <-- LOL
      (CGLPixelFormatAttribute) 0
    };

    CGLContextObj CurrentGLContext = CGLGetCurrentContext();

    CGLPixelFormatObj PixelFormatObject = NULL;
    long PixelFormatNumber = 0;
    CGLChoosePixelFormat(PixelFormatAttributes, &PixelFormatObject, (GLint *) &PixelFormatNumber);

    CGLContextObj CreatedGLContext = NULL;

    if (PixelFormatObject) {
      CGLCreateContext(PixelFormatObject, NULL, &CreatedGLContext);
      CGLDestroyPixelFormat(PixelFormatObject);
      CGLSetCurrentContext(CreatedGLContext);

      if (CreatedGLContext) {
        const GLubyte *ExtensionsString = glGetString(GL_EXTENSIONS);

        const GLubyte *RendererString = glGetString(GL_RENDERER);
        CopyNBytes((unsigned char *) DisplayCapabilities[DisplayIndex].RendererName, (unsigned char *) RendererString, 255);

        const GLubyte *VersionString = glGetString(GL_VERSION);
        CopyNBytes((unsigned char *) DisplayCapabilities[DisplayIndex].RendererVersion, (unsigned char *) VersionString, 255);

        const GLubyte *VendorString = glGetString(GL_VENDOR);
        CopyNBytes((unsigned char *) DisplayCapabilities[DisplayIndex].RendererVendor, (unsigned char *) VendorString, 255);

        int VersionStringIndex = 0;
        unsigned short ShiftValue = 8;

        unsigned char CurrentVersionStringCharacter = VersionString[VersionStringIndex];
        while (('0' <= CurrentVersionStringCharacter && CurrentVersionStringCharacter <= '9') ||
               CurrentVersionStringCharacter == '.') {

          if ('0' <= CurrentVersionStringCharacter && CurrentVersionStringCharacter <= '9') {
            DisplayCapabilities[DisplayIndex].GLVersion += (CurrentVersionStringCharacter - '0') << ShiftValue;
            ShiftValue -= 4;
          }

          VersionStringIndex++;
          CurrentVersionStringCharacter = VersionString[VersionStringIndex];
        }

        // The following is deprecated post-fixed pipeline versions of OpenGL
        glGetIntegerv(GL_MAX_TEXTURE_UNITS, 
                      (int *) &DisplayCapabilities[DisplayIndex].TextureUnits);
        glGetIntegerv(GL_MAX_TEXTURE_SIZE,
                      (int *) &DisplayCapabilities[DisplayIndex].MaxTextureSize); 
        glGetIntegerv(GL_MAX_3D_TEXTURE_SIZE, 
                      (int *) &DisplayCapabilities[DisplayIndex].Max3DTextureSize); 
        glGetIntegerv(GL_MAX_CUBE_MAP_TEXTURE_SIZE, 
                      (int *) &DisplayCapabilities[DisplayIndex].MaxCubeMapTextureSize);
        glGetIntegerv(GL_MAX_RECTANGLE_TEXTURE_SIZE_ARB, 
                      (int *) &DisplayCapabilities[DisplayIndex].MaxRectTextureSize);

        DisplayCapabilities[DisplayIndex].SpecularVector = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_specular_vector",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TransformHint = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_transform_hint",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].AuxDepthStencil = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_aux_depth_stencil",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].ClientStorage = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_client_storage",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].ElementArray = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_element_array",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].Fence = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_fence",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].FloatPixels = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_float_pixels",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].FlushBufferRange = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_flush_buffer_range",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].FlushRender = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_flush_render",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].ObjectPurgeable = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_object_purgeable",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].PackedPixels = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_packed_pixels",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].PixelBuffer = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_pixel_buffer",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].SpecularVector = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_specular_vector",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureRange = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_texture_range",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TransformHint = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_transform_hint",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].VAO = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_vertex_array_object",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].VAR = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_vertex_array_range",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].VPEvaluators = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_vertex_program_evaluators",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].YCbCr = gluCheckExtension(
          (const GLubyte *) "GL_APPLE_ycbcr_422",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].DepthTexture = gluCheckExtension(
          (const GLubyte *) "GL_ARB_depth_texture",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].DrawBuffers = gluCheckExtension(
          (const GLubyte *) "GL_ARB_draw_buffers",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].FragmentProgram = gluCheckExtension(
          (const GLubyte *) "GL_ARB_fragment_program",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].FragmentProgramShadow = gluCheckExtension(
          (const GLubyte *) "GL_ARB_fragment_program_shadow",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].FragmentShader = gluCheckExtension(
          (const GLubyte *) "GL_ARB_fragment_shader",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].HalfFloatPixel = gluCheckExtension(
          (const GLubyte *) "GL_ARB_half_float_pixel",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].Imaging = gluCheckExtension(
          (const GLubyte *) "GL_ARB_imaging",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].Multisample = gluCheckExtension(
          (const GLubyte *) "GL_ARB_multisample",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].Multitexture = gluCheckExtension(
          (const GLubyte *) "GL_ARB_multitexture",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].OcclusionQuery = gluCheckExtension(
          (const GLubyte *) "GL_ARB_occlusion_query",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].PBO = gluCheckExtension(
          (const GLubyte *) "GL_ARB_pixel_buffer_object",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].PointParameters = gluCheckExtension(
          (const GLubyte *) "GL_ARB_point_parameters",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].PointSprite = gluCheckExtension(
          (const GLubyte *) "GL_ARB_point_sprite",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].ShaderObjects = gluCheckExtension(
          (const GLubyte *) "GL_ARB_shader_objects",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].ShaderTextureLOD = gluCheckExtension(
          (const GLubyte *) "GL_ARB_shader_texture_lod",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].ShadingLanguage100 = gluCheckExtension(
          (const GLubyte *) "GL_ARB_shading_language_100",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].Shadow = gluCheckExtension(
          (const GLubyte *) "GL_ARB_shadow",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].ShadowAmbient = gluCheckExtension(
          (const GLubyte *) "GL_ARB_shadow_ambient",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureBorderClamp = gluCheckExtension(
          (const GLubyte *) "GL_ARB_texture_border_clamp",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureCompress = gluCheckExtension(
          (const GLubyte *) "GL_ARB_texture_compress",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureCubeMap = gluCheckExtension(
          (const GLubyte *) "GL_ARB_texture_cube_map",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureEnvAdd = gluCheckExtension(
          (const GLubyte *) "GL_ARB_texture_env_add",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureEnvCombine = gluCheckExtension(
          (const GLubyte *) "GL_ARB_texture_env_combine",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureEnvCrossbar = gluCheckExtension(
          (const GLubyte *) "GL_ARB_texture_env_crossbar",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureEnvDot3 = gluCheckExtension(
          (const GLubyte *) "GL_ARB_texture_env_dot3",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureFloat = gluCheckExtension(
          (const GLubyte *) "GL_ARB_texture_float",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureMirroredRepeat = gluCheckExtension(
          (const GLubyte *) "GL_ARB_texture_mirrored_repeat",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureNonPowerOfTwo = gluCheckExtension(
          (const GLubyte *) "GL_ARB_texture_non_power_of_two",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureRectangleARB = gluCheckExtension(
          (const GLubyte *) "GL_ARB_texture_rectangle",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TransposeMatrix = gluCheckExtension(
          (const GLubyte *) "GL_ARB_transpose_matrix",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].VertexBlend = gluCheckExtension(
          (const GLubyte *) "GL_ARB_vertex_blend",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].VBO = gluCheckExtension(
          (const GLubyte *) "GL_ARB_vertex_buffer_object",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].VertexProgram = gluCheckExtension(
          (const GLubyte *) "GL_ARB_vertex_program",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].VertexShader = gluCheckExtension(
          (const GLubyte *) "GL_ARB_vertex_shader",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].WindowPos = gluCheckExtension(
          (const GLubyte *) "GL_ARB_window_pos",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].ArrayRevCompsIn4Bytes = gluCheckExtension(
          (const GLubyte *) "GL_ATI_array_rev_comps_in_4_bytes",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].BlendEquationSeparateATI = gluCheckExtension(
          (const GLubyte *) "GL_ATI_blend_equation_separate",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].BlendWeightedMinmax = gluCheckExtension(
          (const GLubyte *) "GL_ATI_blend_weighted_minmax",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].PNTriangles = gluCheckExtension(
          (const GLubyte *) "GL_ATI_pn_triangles",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].PointCullMode = gluCheckExtension(
          (const GLubyte *) "GL_ATI_point_cull_mode",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].SeparateStencil = gluCheckExtension(
          (const GLubyte *) "GL_ATI_separate_stencil",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextFragmentShader = gluCheckExtension(
          (const GLubyte *) "GL_ATI_text_fragment_shader",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureCompression3DC = gluCheckExtension(
          (const GLubyte *) "GL_ATI_texture_compression_3dc",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureEnvCombine3 = gluCheckExtension(
          (const GLubyte *) "GL_ATI_texture_env_combine3",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureFloatATI = gluCheckExtension(
          (const GLubyte *) "GL_ATI_texture_float",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureMirrorOnce = gluCheckExtension(
          (const GLubyte *) "GL_ATI_texture_mirror_once",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].ABGR = gluCheckExtension(
          (const GLubyte *) "GL_EXT_abgr",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].BGRA = gluCheckExtension(
          (const GLubyte *) "GL_EXT_bgra",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].BlendColor = gluCheckExtension(
          (const GLubyte *) "GL_EXT_blend_color",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].BlendEquationSeparate = gluCheckExtension(
          (const GLubyte *) "GL_EXT_blend_equation_separate",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].BlendFuncSeparate = gluCheckExtension(
          (const GLubyte *) "GL_EXT_blend_func_separate",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].BlendMinmax = gluCheckExtension(
          (const GLubyte *) "GL_EXT_blend_minmax",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].BlendSubtract = gluCheckExtension(
          (const GLubyte *) "GL_EXT_blend_subtract",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].ClipVolumeHint = gluCheckExtension(
          (const GLubyte *) "GL_EXT_clip_volume_hint",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].ColorSubtable = gluCheckExtension(
          (const GLubyte *) "GL_EXT_color_subtable",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].CVA = gluCheckExtension(
          (const GLubyte *) "GL_EXT_compiled_vertex_array",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].DepthBoundsTest = gluCheckExtension(
          (const GLubyte *) "GL_EXT_depth_bounds_test",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].Convolution = gluCheckExtension(
          (const GLubyte *) "GL_EXT_convolution",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].DrawRangeElements = gluCheckExtension(
          (const GLubyte *) "GL_EXT_draw_range_elements",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].FogCoord = gluCheckExtension(
          (const GLubyte *) "GL_EXT_fog_coord",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].FramebufferBlit = gluCheckExtension(
          (const GLubyte *) "GL_EXT_framebuffer_blit",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].FBO = gluCheckExtension(
          (const GLubyte *) "GL_EXT_framebuffer_object",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].GeometryShader4 = gluCheckExtension(
          (const GLubyte *) "GL_EXT_geometry_shader4",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].GPUProgramParameters = gluCheckExtension(
          (const GLubyte *) "GL_EXT_gpu_program_parameters",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].GPUShader4 = gluCheckExtension(
          (const GLubyte *) "GL_EXT_gpu_shader4",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].Histogram = gluCheckExtension(
          (const GLubyte *) "GL_EXT_histogram",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].PackedDepthStencil = gluCheckExtension(
          (const GLubyte *) "GL_EXT_packed_depth_stencil",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].MultiDrawArrays = gluCheckExtension(
          (const GLubyte *) "GL_multi_draw_arrays",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].PalettedTexture = gluCheckExtension(
          (const GLubyte *) "GL_EXT_paletted_texture",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].RescaleNormal = gluCheckExtension(
          (const GLubyte *) "GL_EXT_rescale_normal",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].SecondaryNormal = gluCheckExtension(
          (const GLubyte *) "GL_EXT_secondary_normal",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].SeparateSpecularColor = gluCheckExtension(
          (const GLubyte *) "GL_EXT_separate_specular_color",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].ShadowFuncs = gluCheckExtension(
          (const GLubyte *) "GL_EXT_shadow_funcs",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].SharedTexturePalette = gluCheckExtension(
          (const GLubyte *) "GL_EXT_shared_texture_palette",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].StencilTwoSide = gluCheckExtension(
          (const GLubyte *) "GL_EXT_stencil_two_side",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].StencilWrap = gluCheckExtension(
          (const GLubyte *) "GL_EXT_stencil_wrap",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureCompressionDXT1 = gluCheckExtension(
          (const GLubyte *) "GL_EXT_texture_compression_dxt1",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].Texture3D = gluCheckExtension(
          (const GLubyte *) "GL_EXT_texture3D",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureCompressionS3TC = gluCheckExtension(
          (const GLubyte *) "GL_EXT_texture_compression_s3tc",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureFilterAnisotropic = gluCheckExtension(
          (const GLubyte *) "GL_EXT_texture_filter_anisotropic",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureLODBias = gluCheckExtension(
          (const GLubyte *) "GL_EXT_texture_lod_bias",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureMirrorClamp = gluCheckExtension(
          (const GLubyte *) "GL_EXT_texture_mirror_clamp",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureRectangle = gluCheckExtension(
          (const GLubyte *) "GL_EXT_texture_rectangle",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureSRGB = gluCheckExtension(
          (const GLubyte *) "GL_EXT_texture_sRGB",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TransformFeedback = gluCheckExtension(
          (const GLubyte *) "GL_EXT_transform_feedback",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].ConvolutionBorderModes = gluCheckExtension(
          (const GLubyte *) "GL_HP_convolution_border_modes",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].RasterposClip = gluCheckExtension(
          (const GLubyte *) "GL_IBM_rasterpos_clip",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].BlendSquare = gluCheckExtension(
          (const GLubyte *) "GL_NV_blend_square",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].DepthClamp = gluCheckExtension(
          (const GLubyte *) "GL_NB_depth_clamp",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].FogDistance = gluCheckExtension(
          (const GLubyte *) "GL_NV_fog_distance",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].LightMaxExponent = gluCheckExtension(
          (const GLubyte *) "GL_NV_light_max_exponent",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].MultisampleFilterHint = gluCheckExtension(
          (const GLubyte *) "GL_NV_multisample_filter_hint",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].PointSpriteNV = gluCheckExtension(
          (const GLubyte *) "GL_NV_point_sprite",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].RegisterCombiners = gluCheckExtension(
          (const GLubyte *) "GL_NV_register_combiners",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].RegisterCombiners2 = gluCheckExtension(
          (const GLubyte *) "GL_NV_register_combiners2",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TexgenReflection = gluCheckExtension(
          (const GLubyte *) "GL_NV_texgen_reflection",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureEnvCombine4 = gluCheckExtension(
          (const GLubyte *) "GL_NV_texture_env_combine4",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureShader = gluCheckExtension(
          (const GLubyte *) "GL_NV_texture_shader",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureShader2 = gluCheckExtension(
          (const GLubyte *) "GL_NV_texture_shader2",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureShader3 = gluCheckExtension(
          (const GLubyte *) "GL_NV_texture_shader3",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].GenerateMipmap = gluCheckExtension(
          (const GLubyte *) "GL_SGIS_generate_mipmap",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureEdgeClamp = gluCheckExtension(
          (const GLubyte *) "GL_SGIS_texture_edge_clamp",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].TextureLOD = gluCheckExtension(
          (const GLubyte *) "GL_SGIS_texture_lod",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].ColorMatrix = gluCheckExtension(
          (const GLubyte *) "GL_SGIS_color_matrix",
          ExtensionsString);
        DisplayCapabilities[DisplayIndex].ColorTable = gluCheckExtension(
          (const GLubyte *) "GL_SGIS_color_table",
          ExtensionsString);

        CGLDestroyContext(CurrentGLContext);
      }
    }

    CGLSetCurrentContext(CreatedGLContext);
  }
}
