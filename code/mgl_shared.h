#ifndef MGL_SHARED_H
#define MGL_SHARED_H

#include <stddef.h>
#include <stdlib.h>

#define internal static
#define global_variable static

unsigned long
CopyNBytes(unsigned char *Destination, unsigned char *Source, unsigned long ByteNumber);

void *
MemoryAlloc(size_t ItemNumber, size_t ItemSize);
void
MemoryFree(void *MemoryPointer);

#endif /* MGL_SHARED_H */
