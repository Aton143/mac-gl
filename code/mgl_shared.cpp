#include "mgl_shared.h"

unsigned long
CopyNBytes(unsigned char *Destination,
           unsigned char *Source,
           unsigned long ByteNumber) {
  if ((Source == NULL) || (Destination == NULL)) {
    return 0;
  }

  for (unsigned long ByteIndex = 0; ByteIndex < ByteNumber; ByteIndex++) {
    Destination[ByteIndex] = Source[ByteIndex];
  }

  return ByteNumber;
}

void *
MemoryAlloc(size_t ItemNumber,
            size_t ItemSize) {
  return (void *) calloc(ItemNumber, ItemSize);
}

void
MemoryFree(void *MemoryPointer) {
  free(MemoryPointer);
}
