#ifndef MAC_GL_OPEN_GL_VIEW
#define MAC_GL_OPEN_GL_VIEW

#import <AppKit/AppKit.h>

#import <OpenGL/gl.h>
#import <OpenGL/glext.h>
#import <OpenGL/glu.h>

#include "mgl_shared.h"

NSOpenGLPixelFormat *
GetBasicPixelFormat();

@interface mgl_gl_view : NSOpenGLView {
}

- (void) ResizeGLViewport;

/*
- (void) keyDown: (NSEvent *) Event;
- (void) mouseDown: (NSEvent *) Event;
- (void) rightMouseDown: (NSEvent *) Event;
- (void) otherMouseDown: (NSEvent *) Event;
- (void) mouseUp: (NSEvent *) Event;
- (void) rightMouseUp: (NSEvent *) Event;
- (void) otherMouseUp: (NSEvent *) Event;
- (void) mouseDragged: (NSEvent *) Event;
- (void) scrollWheel: (NSEvent *) Event;
- (void) rightMouseDragged: (NSEvent *) Event;
- (void) otherMouseDragged: (NSEvent *) Event;
*/

- (void) drawRect: (NSRect) DrawRectangle;

- (void) PrepareOpenGL;

- (void) update; // moved or resized

- (BOOL) acceptsFirstResponder;
- (BOOL) becomeFirstResponder;
- (BOOL) resignFirstResponder;

- (id) initWithFrame: (NSRect) FrameRectangle;
- (void) awakeFromNib;

@end

#endif /* MAC_GL_OPEN_GL_VIEW */
