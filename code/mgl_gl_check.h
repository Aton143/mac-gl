#ifndef MGL_GL_CHECK_H
#define MGL_GL_CHECK_H

#include <ApplicationServices/ApplicationServices.h>

#include <OpenGL/OpenGL.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <OpenGL/glext.h>

#include <string.h>

#include "mgl_shared.h"

#define MAX_DISPLAY_COUNT 32

#define APPLE_24_BIT_DEPTH_RGB  0x208
#define APPLE_64_BIT_DEPTH_RGB  0x210
#define APPLE_128_BIT_DEPTH_RGB 0x220

typedef struct {
  // add capabilities as required
  CGDirectDisplayID CGDisplayID; // Core Graphics Display ID
  CGOpenGLDisplayMask CGLDisplayMask; // CGL display mask

  // current (at time of look up device geometry)
  long DeviceWidth; // pixel width
  long DeviceHeight; // pixel height
  // long DeviceOriginX; // left location of device (rel to main)
  // long DeviceOriginY; // upper location of device (rel to main)

  short DevicePixelDepth; // pixel depth in bits
  short DeviceRefreshRate; // integer refresh rate in Hz

  // Renderer info
  int DeviceVRAMSize; // video memory in bytes
  int DeviceTextureRAMSize; // uses current mode

  long RendererID; // renderer ID
  char RendererName[256]; // name of hardware renderer
  char RendererVendor[256]; // name of hardware vendor
  char RendererVersion[256]; // name of hardware renderer ver.

  bool FullScreenCapable; // does device support full screen
  // can add more device specs

  // Renderer capabilities
  long TextureUnits; // max no. of texture units
  long MaxTextureSize; // max 1/2D texture size supported
  long Max3DTextureSize; // above for 3D
  long MaxCubeMapTextureSize; // max cube map texture size
  long MaxRectTextureSize; // max rect. texture size

  // OpenGL version support
  unsigned short GLVersion;

  // Functionality
  bool AuxDepthStencil;          // GL_APPLE_aux_depth_stencil
  bool ClientStorage;            // GL_APPLE_client_storage
  bool ElementArray;             // GL_APPLE_element_array
  bool Fence;                    // GL_APPLE_fence
  bool FloatPixels;              // GL_APPLE_float_pixels
  bool FlushBufferRange;         // GL_APPLE_flush_buffer_range
  bool FlushRender;              // GL_APPLE_flush_render
  bool ObjectPurgeable;          // GL_APPLE_object_purgeable
  bool PackedPixels;             // GL_APPLE_packed_pixels
  bool PixelBuffer;              // GL_APPLE_pixel_buffer
  bool SpecularVector;           // GL_APPLE_specular_vector
  bool TextureRange;             // GL_APPLE_texture_range
  bool TransformHint;            // GL_APPLE_transorm_hint
  bool VAO;                      // GL_APPLE_vertex_array_object
  bool VAR;                      // GL_APPLE_vertex_array_range
  bool VPEvaluators;             // GL_APPLE_vertex_program_evaluators
  bool YCbCr;                    // GL_APPLE_ycbcr_422 (YUV textures)
  bool DepthTexture;             // GL_ARB_depth_texture
  bool DrawBuffers;              // GL_ARB_draw_buffers
  bool FragmentProgram;          // GL_ARB_fragment_program
  bool FragmentProgramShadow;    // GL_ARB_fragment_program_shadow
  bool FragmentShader;           // GL_ARB_fragment_shader
  bool HalfFloatPixel;           // GL_ARB_half_float_pixel
  bool Imaging;                  // GL_ARB_imaging
  bool Multisample;              // GL_ARB_multisample
  bool Multitexture;             // GL_ARB_multitexture
  bool OcclusionQuery;           // GL_ARB_occlusion_query
  bool PBO;                      // GL_ARB_pixel_buffer_object
  bool PointParameters;          // GL_ARB_point_parameters
  bool PointSprite;              // GL_ARB_point_sprite
  bool ShaderObjects;            // GL_ARB_shader_objects
  bool ShaderTextureLOD;         // GL_ARB_shader_texture_lod
  bool ShadingLanguage100;       // GL_ARB_shading_language_100
  bool Shadow;                   // GL_ARB_shadow
  bool ShadowAmbient;            // GL_ARB_shadow_ambient
  bool TextureBorderClamp;       // GL_ARB_texture_border_clamp
  bool TextureCompress;          // GL_ARB_texture_compress
  bool TextureCubeMap;           // GL_ARB_texture_cube_map
  bool TextureEnvAdd;            // GL_ARB_texture_env_add, (GL_EXT)
  bool TextureEnvCombine;        // GL_ARB_texture_env_combine
  bool TextureEnvCrossbar;       // GL_ARB_texture_env_crossbar
  bool TextureEnvDot3;           // GL_ARB_texture_env_dot3
  bool TextureFloat;             // GL_ARB_texture_float
  bool TextureMirroredRepeat;    // GL_ARB_texture_mirrored_repeat
  bool TextureNonPowerOfTwo;     // GL_ARB_non_power_of_two
  bool TextureRectangleARB;      // GL_ARB_texture_rectangle
  bool TransposeMatrix;          // GL_ARB_transpose_matrix
  bool VertexBlend;              // GL_ARB_vertex_blend
  bool VBO;                      // GL_ARB_vertex_buffer_object
  bool VertexProgram;            // GL_ARB_vertex_program
  bool VertexShader;             // GL_ARB_vertex_shader
  bool WindowPos;                // GL_ARB_window_pos
  bool ArrayRevCompsIn4Bytes;    // GL_ATI_array_rev_comps_in_4_bytes
  bool BlendEquationSeparateATI; // GL_ATI_blend_equation_separate
  bool BlendWeightedMinmax;      // GL_ATI_blend_weighted_minmax
  bool PNTriangles;              // GL_ATI(X)_pn_triangles
  bool PointCullMode;            // GL_ATI_point_cull_mode
  bool SeparateStencil;          // GL_ATI_separate_stencil
  bool TextFragmentShader;       // GL_ATI_text_fragment_shader
  bool TextureCompression3DC;    // GL_ATI_texture_compression_3dc
  bool TextureEnvCombine3;       // GL_ATI_texture_env_combine3
  bool TextureFloatATI;          // GL_ATI_texture_float
  bool TextureMirrorOnce;        // GL_ATI_texture_mirror_once
  bool ABGR;                     // GL_EXT_abgr
  bool BGRA;                     // GL_EXT_bgra
  bool BlendColor;               // GL_EXT_blend_color
  bool BlendEquationSeparate;    // GL_EXT_blend_equation_separate
  bool BlendFuncSeparate;        // GL_EXT_blend_func_separate
  bool BlendMinmax;              // GL_EXT_blend_minmax
  bool BlendSubtract;            // GL_EXT_blend_subtract
  bool ClipVolumeHint;           // GL_EXT_clip_volume_hint
  bool ColorSubtable;            // GL_EXT_color_subtable
  bool CVA;                      // GL_EXT_compiled_vertex_array
  bool DepthBoundsTest;          // GL_EXT_depth_bounds_test
  bool Convolution;              // GL_EXT_convolution
  bool DrawRangeElements;        // GL_EXT_draw_range_elements
  bool FogCoord;                 // GL_EXT_fog_coord
  bool FramebufferBlit;          // GL_EXT_framebuffer_blit
  bool FBO;                      // GL_EXT_framebuffer_object
  bool GeometryShader4;          // GL_EXT_geometry_shader4
  bool GPUProgramParameters;     // GL_EXT_gpu_program_parameters
  bool GPUShader4;               // GL_EXT_gpu_shader4
  bool Histogram;                // GL_EXT_histogram
  bool PackedDepthStencil;       // GL_EXT_packed_depth_stencil
  bool MultiDrawArrays;          // GL_EXT_multi_draw_arrays
  bool PalettedTexture;          // GL_EXT_paletted_texture
  bool RescaleNormal;            // GL_EXT_rescale_normal
  bool SecondaryNormal;          // GL_EXT_secondary_normal
  bool SeparateSpecularColor;    // GL_EXT_separate_specular_color
  bool ShadowFuncs;              // GL_EXT_shadow_funcs
  bool SharedTexturePalette;     // GL_EXT_shared_texture_palette
  bool StencilTwoSide;           // GL_EXT_stencil_two_side
  bool StencilWrap;              // GL_EXT_stencil_wrap
  bool TextureCompressionDXT1;   // GL_EXT_texture_compression_dxt1
  bool Texture3D;                // GL_EXT_texture3D
  bool TextureCompressionS3TC;   // GL_EXT_texture_compression_s3tc
  bool TextureFilterAnisotropic; // GL_EXT_texture_filter_anisotropic
  bool TextureLODBias;           // GL_EXT_texture_lod_bias
  bool TextureMirrorClamp;       // GL_EXT_texture_mirror_clamp
  bool TextureRectangle;         // GL_EXT_texture_rectangle
  bool TextureSRGB;              // GL_EXT_texture_sRGB
  bool TransformFeedback;        // GL_EXT_transform_feedback
  bool ConvolutionBorderModes;   // GL_HP_convolution_border_modes
  bool RasterposClip;            // GL_IBM_rasterpos_clip
  bool BlendSquare;              // GL_NV_blend_square
  bool DepthClamp;               // GL_NV_depth_clamp
  bool FogDistance;              // GL_NV_fog_distance
  bool LightMaxExponent;         // GL_NV_light_max_exponent
  bool MultisampleFilterHint;    // GL_NV_multisample_filter_hint
  bool PointSpriteNV;            // GL_NV_point_sprite
  bool RegisterCombiners;        // GL_NV_register_combiners
  bool RegisterCombiners2;       // GL_NV_register_combiners2
  bool TexgenReflection;         // GL_NV_texgen_reflection
  bool TextureEnvCombine4;       // GL_NV_texgen_env_combine4
  bool TextureShader;            // GL_NV_texture_shader
  bool TextureShader2;           // GL_NV_texture_shader2
  bool TextureShader3;           // GL_NV_texture_shader3
  bool GenerateMipmap;           // GL_SGIS_generate_mipmap
  bool TextureEdgeClamp;         // GL_SGIS_texture_edge_clamp
  bool TextureLOD;               // GL_SGIS_texture_lod
  bool ColorMatrix;              // GL_SGIS_color_matrix
  bool ColorTable;               // GL_SGIS_color_table
} mgl_gl_capabilities;

// checks to see if capabilities have changed without being
// too heavyweight; returns 1 if changes 0 if not
// checks number of displays, displayID, displayMask,
// each display geometry and renderer VRAM and ID
unsigned char
mgl_HaveOpenGLCapabilitiesChanged(mgl_gl_capabilities *MainDisplayCapabilities, CGDisplayCount DisplayCount);

// Walks all active displays and gathers information about their
// hardware renderer

// Actual number of diplays filled in is returned
// If MaxDisplays is 0, then return DisplayCount
void
mgl_CheckOpenGLCapabilities(CGDisplayCount MaxDisplays, mgl_gl_capabilities *DisplayCapabilities, CGDisplayCount *DisplayCount);
#endif /* MGL_GL_CHECK_H */
