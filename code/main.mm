#import <Cocoa/Cocoa.h>
#import <AppKit/AppKit.h>

#include "mgl_shared.h"
#include "mgl_gl_check.h"
#include "mgl_gl_view.h"

int main(int ArgCount, char *ArgValues[]) {
  // mgl_gl_capabilities DisplayCapabilities = {};
  // mgl_HaveOpenGLCapabilitiesChanged(&DisplayCapabilities, (CGDisplayCount) 1);
  // glGetString(GL_EXTENSIONS);

  return NSApplicationMain(ArgCount, (const char **) ArgValues);
}
