#ifndef MAC_GL_APP_DELEGATE_H
#define MAC_GL_APP_DELEGATE_H

#import <Cocoa/Cocoa.h>

@interface mgl_app_delegate : NSObject
{

}
- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)Application;

@end

#endif /* MAC_GL_APP_DELEGATE_H */
