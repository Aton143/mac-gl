#import "mgl_gl_view.h"
#import "mgl_gl_check.h"

/* OpenGL Capabilities */
global_variable mgl_gl_capabilities *DisplayCapabilities = NULL;
global_variable CGDisplayCount DisplayNumber = 0;

internal void
GetCurrentCapabilities() {
  if (DisplayCapabilities && mgl_HaveOpenGLCapabilitiesChanged(DisplayCapabilities, DisplayNumber)) {
    MemoryFree(DisplayCapabilities);
    DisplayCapabilities = NULL;
  }
  if (DisplayCapabilities == NULL) {
    // get the number of displays - Make this a separate function???
    // TODO(antonio): mgl_GetDisplayNumber()
    mgl_CheckOpenGLCapabilities(0, NULL, &DisplayNumber);
    DisplayCapabilities = (mgl_gl_capabilities *) MemoryAlloc(DisplayNumber, sizeof(mgl_gl_capabilities));
    mgl_CheckOpenGLCapabilities(DisplayNumber, DisplayCapabilities, &DisplayNumber);
  }
}

NSOpenGLPixelFormat *
GetBasicPixelFormat() {
  NSOpenGLPixelFormatAttribute PixelFormatAttributes[] = {
    NSOpenGLPFAWindow, // only renderers that are capable of rendering to the window are considered
    NSOpenGLPFAAccelerated,  // only hardware-accelerated renderers are considered
    NSOpenGLPFAFullScreen, // only full-screen-capable renderers are considered 
    NSOpenGLPFADoubleBuffer, // only double-buffer-capable renderers are considered
    (NSOpenGLPixelFormatAttribute) nil
  };

  NSOpenGLPixelFormat *OpenGLPixelFormat = [NSOpenGLPixelFormat alloc];
  [OpenGLPixelFormat initWithAttributes:PixelFormatAttributes];
  [OpenGLPixelFormat autorelease];
  return OpenGLPixelFormat;
}

@implementation mgl_gl_view

- (void) ResizeGLViewport {
  NSRect WindowRectangle = [self bounds]; // from NSView (NSView -> NSOpenGLView -> mgl_gl_view)  
  // TODO(antonio): don't call glViewport if window has not been resized
  glViewport(0, 0, WindowRectangle.size.width, WindowRectangle.size.height);
}

- (void) drawRect: (NSRect) DrawRectangle {
  [self ResizeGLViewport];
  
  glClear(GL_COLOR_BUFFER_BIT);

  if ([self inLiveResize]) {
    glFlush();
  }
}

- (void) PrepareOpenGL {
  long swapInt = 1;

  NSOpenGLContext *OpenGLContext = [self openGLContext];

  // enable VSync
  [OpenGLContext setValues:(const GLint *) &swapInt
              forParameter:NSOpenGLContextParameterSwapInterval];

  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

- (void) update {
  [super update];

  if (![self inLiveResize]) {
    GetCurrentCapabilities();
  }
}

- (id) initWithFrame: (NSRect) FrameRectangle {
  NSOpenGLPixelFormat *OpenGLPixelFormat = GetBasicPixelFormat();

  self = [super initWithFrame:FrameRectangle
                  pixelFormat:OpenGLPixelFormat];
  return self;
}

- (BOOL) acceptsFirstResponder {
  return YES;
}

- (BOOL) becomeFirstResponder {
  return YES;
}
- (BOOL) resignFirstResponder {
  return YES;
}

- (void) awakeFromNib {
  GetBasicPixelFormat();
}

@end
