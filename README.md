# mac-gl

Initializing OpenGL on macOS without external libraries

## Notes

The following are based on [sample code from Apple](https://developer.apple.com/library/archive/samplecode/CocoaGL/Listings/GLCheck_c.html#//apple_ref/doc/uid/DTS10004501-GLCheck_c-DontLinkElementID_8)

### Obtaining device capabilities
Device capabilities may be accessed using functions offered by the [Core Graphics API](https://developer.apple.com/documentation/coregraphics/quartz_display_services?language=objc#1656418). The following work for macOS 10.6+ (Snow Leopard+):
1. Get display list `CGGetActiveDisplayList`
2. Get display mode for each `CGDisplayCopyDisplayMode`
3. Get desired capabilities using functions `CGDisplayModeGetHeight`
